PySpark application for generating section image recommendations.

## Using Conda

To use this package and its dependencies on executors, create a Conda environment and pack it into an archive file:
```
conda-create-stacked imagerec_env
conda activate imagerec_env
pip install .  # Or use poetry install
conda pack -o imagerec_env.tar.gz --force --ignore-editable-packages
```
This archive file can then be directly passed and unpacked to enable the environment on executors by leveraging the --archives option for spark-submit.

## Extracting images from articles

Example invocation to extract images per section:

```bash
PYSPARK_DRIVER_PYTHON=python \
PYSPARK_PYTHON=./environment/bin/python \
spark2-submit \
 --master yarn \
 --executor-cores 4 \
 --executor-memory 8G \
 --conf spark.dynamicAllocation.maxExecutors=128 \
 --conf spark.sql.shuffle.partitions=1024 \
 --archives imagerec_env.tar.gz#environment \
 imagerec/article_images.py \
 --wikitext-snapshot 2022-06 \
 --item-page-link-snapshot 2022-06-27 \
 --output /user/mnz/output/article_images.parquet \
 --wp-codes af en  # or pass in a list using JSON
```
To display help for all available options:
```
python imagerec/article_images.py -h
```

## Generating recommendations from article images

Example invocation to generate section image recommendations using the extracted article images:
```bash
PYSPARK_DRIVER_PYTHON=python \
PYSPARK_PYTHON=./environment/bin/python \
spark2-submit \
 --master yarn \
 --executor-cores 4 \
 --executor-memory 8G \
 --conf spark.dynamicAllocation.maxExecutors=128 \
 --conf spark.sql.shuffle.partitions=2048 \
 --archives imagerec_env.tar.gz#environment \
 imagerec/recommendation.py \
 --section-images /user/mnz/output/article_images.parquet \
 --section-alignments /user/mnz/secmap_results/aligned_sections_subset/aligned_sections_subset_9.0_2022-02.parquet \
 --output /user/mnz/output/recommendation \
 --wp-codes af en  # or pass in a list using JSON
 --max-target-images 0  # generate recs only for sections with no images
```
To display help for all available options:
```
python imagerec/recommendation.py -h
```
