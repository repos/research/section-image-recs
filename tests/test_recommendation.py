from typing import List

import pandas as pd  # type: ignore
import pytest

from imagerec.article_images import SectionImages
from imagerec.recommendation import (
    Page,
    Recommendation,
    combine_pages,
    filter_source_images,
    generate_image_recommendations,
    load,
    make_recommendations,
    rows_to_pages,
)


@pytest.mark.parametrize(
    "article_images,expected",
    [
        (
            '[{"heading":"foo","images":[]},{"heading":"bar","images":[]}]',
            [
                SectionImages(heading="foo", images=[]),
                SectionImages(heading="bar", images=[]),
            ],
        ),
        (
            '[{"heading":"foo","images":["bar"]},{"heading":"baz","images":["qux"]}]',
            [
                SectionImages(heading="foo", images=["bar"]),
                SectionImages(heading="baz", images=["qux"]),
            ],
        ),
    ],
)
def test_load(article_images: str, expected: List[SectionImages]) -> None:
    assert load(article_images) == expected


@pytest.mark.parametrize(
    "data,expected",
    [
        ([], []),
        (
            [("Q1", "foo", "enwiki", '[{"heading":"bar","images":["baz"]}]')],
            [
                Page(
                    item_id="Q1",
                    page_title="foo",
                    wiki_db="enwiki",
                    section_images=[SectionImages(heading="bar", images=["baz"])],
                )
            ],
        ),
    ],
)
def test_rows_to_pages(data: List[str], expected: List[Page]) -> None:
    df = pd.DataFrame(
        data, columns=["item_id", "page_title", "wiki_db", "article_images"]
    )
    assert rows_to_pages(df) == expected


@pytest.mark.parametrize(
    "source_images,target_images,expected",
    [(["foo", "bar"], ["bar"], ["foo"]), (["OOjs UI icon"], [], [])],
)
def test_filter_source_images(
    source_images: List[str], target_images: List[str], expected: List[str]
) -> None:
    assert filter_source_images(source_images, set(target_images)) == expected


@pytest.mark.parametrize(
    "target_page,source_page,max_target_images,expected",
    [
        (
            Page(
                item_id="Q1",
                page_title="foo",
                wiki_db="arwiki",
                section_images=[SectionImages(heading="lede_section", images=["bar"])],
            ),
            Page(
                item_id="Q1",
                page_title="baz",
                wiki_db="enwiki",
                section_images=[
                    SectionImages(heading="lede_section", images=["quuz"]),
                    SectionImages(heading="qux", images=["quux"]),
                ],
            ),
            5,
            [],
        ),
        (
            Page(
                item_id="Q1",
                page_title="foo",
                wiki_db="afwiki",
                section_images=[
                    SectionImages(heading="lede_section", images=["corge"]),
                    SectionImages(heading="bar", images=["baz"]),
                ],
            ),
            Page(
                item_id="Q1",
                page_title="qux",
                wiki_db="ptwiki",
                section_images=[
                    SectionImages(heading="lede_section", images=["grault"]),
                    SectionImages(
                        heading="quux",
                        images=["baz", "quuz", "OOjs UI icon", "corge"],
                    ),
                ],
            ),
            1,
            [
                Recommendation(
                    item_id="Q1",
                    target_title="foo",
                    target_heading="bar",
                    target_wiki_db="afwiki",
                    source_heading="quux",
                    source_wiki_db="ptwiki",
                    recommended_images=["quuz"],
                )
            ],
        ),
        (
            Page(
                item_id="Q1",
                page_title="foo",
                wiki_db="afwiki",
                section_images=[
                    SectionImages(heading="lede_section", images=["corge"]),
                    SectionImages(heading="bar", images=["baz"]),
                ],
            ),
            Page(
                item_id="Q1",
                page_title="qux",
                wiki_db="ptwiki",
                section_images=[
                    SectionImages(heading="lede_section", images=["grault"]),
                    SectionImages(
                        heading="quux",
                        images=["baz", "quuz", "OOjs UI icon", "corge"],
                    ),
                ],
            ),
            0,
            [],
        ),
    ],
)
def test_make_recommendations(
    target_page: Page,
    source_page: Page,
    max_target_images: int,
    expected: List[Recommendation],
) -> None:
    assert make_recommendations(target_page, source_page, max_target_images) == expected


def test_combine_pages() -> None:
    page1 = Page(
        item_id="Q1",
        page_title="foo",
        wiki_db="afwiki",
        section_images=[
            SectionImages(heading="lede_section", images=["corge"]),
        ],
    )
    page2 = Page(
        item_id="Q1",
        page_title="qux",
        wiki_db="ptwiki",
        section_images=[
            SectionImages(heading="lede_section", images=["grault"]),
        ],
    )
    page3 = Page(
        item_id="Q1",
        page_title="qux",
        wiki_db="eswiki",
        section_images=[
            SectionImages(heading="lede_section", images=["grault"]),
        ],
    )

    target_pages = [page1]
    source_pages = [page1, page2, page3]
    assert combine_pages(target_pages, source_pages) == [(page1, page2), (page1, page3)]

    target_pages = [page1]
    source_pages = [page1]
    assert combine_pages(target_pages, source_pages) == []


@pytest.mark.parametrize(
    "target_wiki_dbs,max_target_images,data,expected_data",
    [
        (
            ["plwiki"],
            1,
            [
                ("Q1", "foo", "plwiki", '[{"heading":"bar","images":["baz"]}]'),
                (
                    "Q1",
                    "bar",
                    "urwiki",
                    '[{"heading":"qux","images":["quuz"]}, {"heading":"corge","images":["quux"]}]',
                ),
            ],
            [
                ("Q1", "foo", "bar", "plwiki", "qux", "urwiki", ["quuz"]),
                ("Q1", "foo", "bar", "plwiki", "corge", "urwiki", ["quux"]),
            ],
        ),
        (
            ["eswiki"],
            5,
            [
                ("Q1", "foo", "plwiki", '[{"heading":"bar","images":["baz"]}]'),
                ("Q1", "bar", "urwiki", '[{"heading":"qux","images":["quz"]}]'),
            ],
            [],
        ),
    ],
)
def test_generate_image_recommendations(
    target_wiki_dbs: List[str],
    max_target_images: int,
    data: List[str],
    expected_data: List[str],
) -> None:
    input_df = pd.DataFrame(
        data, columns=["item_id", "page_title", "wiki_db", "article_images"]
    )
    expected_df = pd.DataFrame(
        expected_data,
        columns=[
            "item_id",
            "target_title",
            "target_heading",
            "target_wiki_db",
            "source_heading",
            "source_wiki_db",
            "recommended_images",
        ],
    )
    pd.testing.assert_frame_equal(
        generate_image_recommendations(target_wiki_dbs, max_target_images, input_df),
        expected_df,
    )
