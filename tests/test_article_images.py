from typing import List, Tuple

import mwparserfromhell as mwp  # type: ignore
import pytest

from imagerec.article_images import get_images, split_section


class TestGetImages:
    @pytest.mark.parametrize(
        "wikitext,expected",
        [
            (
                "[[File:A_portrait_of_A,_by_B–C.PNG|thumb|center|x60px|alt=Foo bar baz|Example image]]",
                ["A_portrait_of_A,_by_B–C.PNG"],
            ),
            (
                "[[File:Example - ita, 1825 - 766672 R (cropped).jpeg|thumb|left|The ''[[example]]]'' image.]]",
                ["Example - ita, 1825 - 766672 R (cropped).jpeg"],
            ),
        ],
    )
    def test_image(self, wikitext: str, expected: List[str]) -> None:
        assert get_images(wikitext) == expected

    @pytest.mark.parametrize(
        "wikitext,expected",
        [
            (
                """{{Infobox book
                    | name           = Anne of Green Gables
                    | image          = FirstPageGreenGables.gif
                    | caption        = The front (first) page of the first edition
                    | website        = https://www.anneofgreengables.com/
                    }}""",
                ["FirstPageGreenGables.gif"],
            ),
            (
                """{{Infobox album
                    | name       = Modal Soul
                    | type       = studio
                    | artist     = [[Nujabes]]
                    | cover      = ModalMusic.jpg
                    }}""",
                ["ModalMusic.jpg"],
            ),
        ],
    )
    def test_infobox(self, wikitext: str, expected: List[str]) -> None:
        assert get_images(wikitext) == expected

    @pytest.mark.parametrize(
        "wikitext,expected",
        [
            (
                """<gallery widths="170" heights="170">
                MET 1984 482 237872.jpg|Seal; 3000–1500 BC; baked [[steatite]]; 2 × 2&nbsp;cm; [[Metropolitan Museum of Art]] (New York City)
                File:Stamp seal and modern impression- unicorn and incense burner (?) MET DP23101 (cropped).jpg|Stamp seal and modern impression: unicorn and incense burner (?); 2600-1900&nbsp;BC; burnt steatite; 3.8 × 3.8 × 1&nbsp;cm; Metropolitan Museum of Art
                Clevelandart 1973.160.jpg|Seal with two-horned bull and inscription; 2010 BC; steatite; overall: 3.2 x 3.2&nbsp;cm; [[Cleveland Museum of Art]] ([[Cleveland]], [[Ohio]], US)
                </gallery>""",
                [
                    "MET 1984 482 237872.jpg",
                    "Stamp seal and modern impression- unicorn and incense burner (?) MET DP23101 (cropped).jpg",
                    "Clevelandart 1973.160.jpg",
                ],
            ),
            (
                """{{Gallery
                   |title=Example gallery
                   |width=160 | height=170
                   |align=center
                   |footer=Example 1
                   |File:An Image taken between 1950-1960.JPG
                    |An example [[image]] taken in the [[1950s]]
                    |alt1=Example alt text for an example image
                   }}""",
                ["An Image taken between 1950-1960.JPG"],
            ),
        ],
    )
    def test_gallery(self, wikitext: str, expected: List[str]) -> None:
        assert get_images(wikitext) == expected

    @pytest.mark.parametrize(
        "wikitext,expected",
        [
            (
                "[[A_portrait_of_A,_by_B–C.PNG|thumb|center|x60px|alt=Foo bar baz|Example image]]",
                [],
            ),
            (
                "[[  Example - ita, 1825 - 766672 R (cropped).jpeg|thumb|left|The ''[[example]]]'' image.]]",
                [],
            ),
        ],
    )
    def test_invalid_prefix(self, wikitext: str, expected: List[str]) -> None:
        assert get_images(wikitext) == expected


@pytest.mark.parametrize(
    "section,is_lede,expected",
    [
        ("{{foo|bar}}Foo\n", True, ("lede_section", "{{foo|bar}}Foo\n")),
        ("==HeadinG==\nFoo bar baz\n", False, ("heading", "Foo bar baz\n")),
        ("== Heading ==\nFoo bar baz\n", False, ("heading", "Foo bar baz\n")),
        (
            "==    Heading    ==\nFoo bar baz\n",
            False,
            ("heading", "Foo bar baz\n"),
        ),
        (
            "==Heading(1-2).==\nFoo bar baz\n",
            False,
            ("heading(1-2)", "Foo bar baz\n"),
        ),
    ],
)
def test_split_section(section: str, is_lede: bool, expected: Tuple[str, str]) -> None:
    assert split_section(mwp.parse(section), is_lede=is_lede) == expected
