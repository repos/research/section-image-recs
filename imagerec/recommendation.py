import argparse
import json

from functools import partial
from itertools import product
from typing import List, NamedTuple, Sequence, Set, Tuple

import pandas as pd  # type: ignore

from pyspark.sql import (  # type: ignore
    DataFrame as SparkDataFrame,
    SparkSession,
    functions as F,
    types as T,
)

from imagerec.article_images import SectionImages
from imagerec.utils import load_wp_codes, wp_code_to_wiki_db


class Page(NamedTuple):
    item_id: str
    page_title: str
    wiki_db: str
    section_images: List[SectionImages]

    @property
    def all_images(self) -> List[str]:
        return [image for s in self.section_images for image in s.images]


class Recommendation(NamedTuple):
    item_id: str
    target_title: str
    target_heading: str
    target_wiki_db: str
    source_heading: str
    source_wiki_db: str
    recommended_images: List[str]


recommendation_schema = T.StructType(
    [
        T.StructField("item_id", T.StringType(), False),
        T.StructField("target_title", T.StringType(), False),
        T.StructField("target_heading", T.StringType(), False),
        T.StructField("target_wiki_db", T.StringType(), False),
        T.StructField("source_heading", T.StringType(), False),
        T.StructField("source_wiki_db", T.StringType(), False),
        T.StructField("recommended_images", T.ArrayType(T.StringType()), False),
    ]
)


def load(article_images: str) -> List[SectionImages]:
    """Deserializes `article_images` struct and converts
    each struct in article images into a `SectionImages`
    instance.
    """
    section_images = json.loads(article_images)
    return list(map(lambda s: SectionImages(**s), section_images))


def rows_to_pages(df: pd.DataFrame) -> List[Page]:
    """Converts each row in `df` to a `Page` instance."""
    rows = df.to_records(index=False)
    return [
        Page(
            item_id=row["item_id"],
            page_title=row["page_title"],
            wiki_db=row["wiki_db"],
            section_images=load(row["article_images"]),
        )
        for row in rows
    ]


def filter_source_images(
    source_images: Sequence[str], target_images: Set[str]
) -> List[str]:
    """Filters out any image in `source_images` if it
    also appear in `target_images` or if it is an icon
    or indicator i.e. has a name like 'OOjs UI icon xyz'
    or 'OOjs UI indicator'.
    """
    return [
        img
        for img in source_images
        if not (img in target_images or img.startswith("OOjs UI"))
    ]


def make_recommendations(
    target_page: Page, source_page: Page, max_target_images: int
) -> List[Recommendation]:
    """Returns a list of image recommendations for `target_page`
    by generating section pairs from `target_page` and `source_page`
    and using filtered images found in `source_page` sections as
    recommended images for corresponding `target_page` section in
    each pair.
    """
    target_images = set(target_page.all_images)
    # filter out sections with images more than max_target_images
    # so that we don't make recommendations for them
    target_section_images = (
        section_images
        for section_images in target_page.section_images
        if len(section_images.images) <= max_target_images
    )
    # generate cartesian product of all sections in target_page and
    # source_page, excluding the lede sections, to make recommendations
    return [
        Recommendation(
            item_id=target_page.item_id,
            target_title=target_page.page_title,
            target_heading=s1.heading,
            target_wiki_db=target_page.wiki_db,
            source_heading=s2.heading,
            source_wiki_db=source_page.wiki_db,
            recommended_images=filter_source_images(s2.images, target_images),
        )
        for s1, s2 in product(target_section_images, source_page.section_images)
        if "lede_section" not in (s1.heading, s2.heading)
    ]


def combine_pages(
    target_pages: Sequence[Page], source_pages: Sequence[Page]
) -> List[Tuple[Page, Page]]:
    """Generates the cartesian product of all pages in `target_pages` and
    `source_pages` making sure that no (target, source) page pair has the
    same language.
    """
    return [
        (target_page, source_page)
        for target_page, source_page in product(target_pages, source_pages)
        if target_page.wiki_db != source_page.wiki_db
    ]


def generate_image_recommendations(
    target_wiki_dbs: Sequence[str], max_target_images: int, df: pd.DataFrame
) -> pd.DataFrame:
    """Generates image recommendations for all wiki_dbs in `target_wiki_dbs`
    in `df` using articles in other languages with the same item_id.
    """
    pages = rows_to_pages(df)
    target_pages = [page for page in pages if page.wiki_db in target_wiki_dbs]
    # Make recommendation for each (target, source) page pair
    # and filter out empty recommendations
    recommendations = [
        recommendation
        for p1, p2 in combine_pages(target_pages, pages)
        for recommendation in make_recommendations(p1, p2, max_target_images)
        if recommendation.recommended_images
    ]
    return pd.DataFrame.from_records(recommendations, columns=Recommendation._fields)


def get_image_recommendations(
    section_images_df: SparkDataFrame,
    target_wiki_dbs: Sequence[str],
    max_target_images: int,
) -> SparkDataFrame:
    """Generates image recommendations for all sections in
    all articles in each target wiki_db using articles in
    other languages with the same item_id in `section_images_df`.
    """
    # a grouped map udf takes f: pd.DataFrame -> pd.DataFrame
    # so by using partial we can take a function that takes
    # arguments other than just a dataframe and create a new
    # one that will work with the grouped map udf.
    generate_image_recommendations_udf = F.pandas_udf(
        partial(generate_image_recommendations, target_wiki_dbs, max_target_images),
        recommendation_schema,
        F.PandasUDFType.GROUPED_MAP,
    )

    # Arrow, used by spark to transfer data to python
    # workers for pandas udf execution, does not support
    # nested StructType hence we convert article_images
    # to json before applying the pandas udf
    section_images_df = section_images_df.withColumn(
        "article_images", F.to_json("article_images")
    )
    recommendations_df = section_images_df.groupBy("item_id").apply(
        generate_image_recommendations_udf
    )
    return recommendations_df


def process_image_recommendations(
    recommendations_df: SparkDataFrame, alignments_df: SparkDataFrame
) -> SparkDataFrame:
    """Filters out recommendation pairs in `recommendations_df` using
    `alignments_df` and combines and de-duplicates recommendations
    per source_wiki_db.
    """
    # join recommendation section pairs and alignments to
    # only get recommendations where the sections are
    # semantically similar
    recommendations_df = recommendations_df.alias("r")
    alignments_df = alignments_df.alias("a")

    target_headings_match = F.col("r.target_heading") == F.col("a.source")
    source_headings_match = F.col("r.source_heading") == F.col("a.target")
    processed_recs_df = recommendations_df.join(
        alignments_df,
        on=((target_headings_match) & (source_headings_match)),
        how="left_semi",
    )

    # combine all recommended images from each section in
    # source_wiki_db for a target heading
    processed_recs_df = processed_recs_df.groupBy(
        [
            "item_id",
            "target_title",
            "target_heading",
            "target_wiki_db",
            "source_wiki_db",
        ]
    )
    processed_recs_df = processed_recs_df.agg(
        F.array_distinct(F.flatten(F.collect_list("recommended_images"))).alias(
            "recommended_images"
        )
    )

    # group all recommended images from each source_wiki_db for
    # one target section into one map
    processed_recs_df = processed_recs_df.groupBy(
        ["item_id", "target_title", "target_heading", "target_wiki_db"]
    )
    processed_recs_df = processed_recs_df.agg(
        F.collect_list(F.create_map("source_wiki_db", "recommended_images")).alias(
            "recommended_images"
        )
    )
    return processed_recs_df


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--section-images",
        required=True,
        metavar="/complete/path/to/file",
        help="The parquet file containing the section images dataframe",
    )
    parser.add_argument(
        "--section-alignments",
        required=True,
        metavar="/complete/path/to/file",
        help="The parquet file containing section alignments",
    )
    parser.add_argument(
        "--output",
        required=True,
        metavar="/complete/path/to/dir",
        help="The path to write the output parquet files to",
    )
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "--wp-codes",
        nargs="*",
        dest="wiki_dbs",
        metavar="wp-code",
        type=wp_code_to_wiki_db,
        help=(
            "Wikipedia codes to generate recommendations for, "
            "separated by spaces e.g. ar en zh-yue"
        ),
    )
    group.add_argument(
        "--wp-codes-file",
        metavar="/complete/path/to/file",
        dest="wiki_dbs",
        help=(
            "The json file containing list of "
            "Wikipedia codes to generate recommendations for"
        ),
        type=load_wp_codes,
    )
    parser.add_argument(
        "--max-target-images",
        metavar="N",
        type=int,
        help=(
            "Maximum number of images that a section being"
            "recommended images should contain. Use 0 if you"
            "want to generate recommendations only for unillustrated"
            "sections."
        ),
    )
    args = parser.parse_args()

    spark = SparkSession.builder.getOrCreate()
    section_images_df = spark.read.parquet(args.section_images)
    alignments_df = spark.read.parquet(args.section_alignments)

    recommendations_df = get_image_recommendations(
        section_images_df, args.wiki_dbs, args.max_target_images
    )
    processed_recs_df = process_image_recommendations(recommendations_df, alignments_df)
    processed_recs_df.write.partitionBy("target_wiki_db").mode("overwrite").format(
        "parquet"
    ).save(args.output)

    spark.stop()


if __name__ == "__main__":
    main()
