import argparse
import logging
import re
import string

from typing import List, NamedTuple, Optional, Tuple

import mwparserfromhell as mwp  # type: ignore

from pyspark.sql import (  # type: ignore
    DataFrame,
    SparkSession,
    functions as F,
    types as T,
)

from imagerec.utils import load_wp_codes, wp_code_to_wiki_db


IMAGE_NAME_PREFIX = r"""
(?:
      =   # equals sign if in a template
    | :   # or a colon if of the form file:foo.jpg
    | \n  # or newline if in a gallery tag
)
"""
ALLOWED_IMAGE_NAME_CHARACTERS = r"""
    # any character excluding carriage return, newline,
    # hash, angle brackets, square brackets, vertical bar,
    # colon, braces and forward slash
    [^\r\n\#\<\>\[\]\|:\{\}/]
"""
IMAGE_RE = re.compile(
    rf"""
    {IMAGE_NAME_PREFIX}                 # prefix
    ({ALLOWED_IMAGE_NAME_CHARACTERS}+)  # filename
    \.(\w+)                             # extension with period
    """,
    re.VERBOSE,
)
IMAGE_EXTENSIONS = [
    "jpg",
    "png",
    "svg",
    "gif",
    "jpeg",
    "tif",
    "bmp",
    "webp",
    "xcf",
]
PUNCTUATION = string.punctuation.replace("()", " ")
WHITESPACE_RE = re.compile(r"\s")


# cloudpickle 0.4.4 bundled with PySpark 2.4.4 does not
# support dataclasses hence the use of NamedTuple
class SectionImages(NamedTuple):
    heading: str
    images: List[str]


section_images_schema = T.ArrayType(
    T.StructType(
        [
            T.StructField("heading", T.StringType(), False),
            T.StructField("images", T.ArrayType(T.StringType()), False),
        ]
    )
)


def split_section(section: str, *, is_lede: bool) -> Tuple[str, str]:
    """Splits section into heading and text."""
    heading, *section_text = (
        ("lede_section", section) if is_lede else section.split("\n", maxsplit=1)
    )
    heading = WHITESPACE_RE.sub(" ", heading)
    normalized_heading = heading.strip(PUNCTUATION).lower()
    [text] = section_text or [""]
    return normalized_heading, text


def get_images(wikitext: str) -> List[str]:
    """Returns all image names in the given wikitext."""
    images = [
        f"{name}.{extension}".strip()
        for name, extension in IMAGE_RE.findall(wikitext)
        if extension.lower() in IMAGE_EXTENSIONS
    ]
    return images


@F.udf(returnType=section_images_schema)
def extract_section_images(wikitext: str) -> Optional[List[SectionImages]]:
    """Extracts images from each section in the wikitext
    including the lede section.
    """
    try:
        sections = mwp.parse(wikitext).get_sections(
            levels=[2], include_headings=True, include_lead=True
        )
        split_sections = [
            split_section(str(section), is_lede=i == 0)
            for i, section in enumerate(sections)
        ]
        return [
            SectionImages(heading=heading, images=get_images(text))
            for heading, text in split_sections
        ]
    except Exception as e:
        logging.exception(f"Couldn't parse article: {wikitext}", e)
        return None


def get_article_images(
    spark: SparkSession,
    wikitext_snapshot: str,
    item_page_link_snapshot: str,
    wiki_dbs: Optional[List[str]],
) -> DataFrame:
    """Returns a PySpark dataframe containing section images
    for all articles in wikipedia projects filtered by `wiki_dbs`.
    """
    articles_df = spark.sql(
        f"""SELECT
                ipl.item_id,
                wt.page_title,
                wt.wiki_db,
                wt.revision_text
            FROM wmf.mediawiki_wikitext_current as wt
            INNER JOIN wmf.wikidata_item_page_link as ipl
                ON wt.page_id = ipl.page_id
                AND wt.wiki_db = ipl.wiki_db
            WHERE wt.snapshot = '{wikitext_snapshot}'
            AND ipl.snapshot = '{item_page_link_snapshot}'
            AND wt.page_namespace = 0
            AND ipl.page_namespace = 0
            AND wt.page_redirect_title = ''
        """
    )

    if wiki_dbs:
        articles_df = articles_df.filter(F.col("wiki_db").isin(wiki_dbs))

    images_df = articles_df.select(
        "item_id",
        "page_title",
        "wiki_db",
        extract_section_images("revision_text").alias("article_images"),
    )
    return images_df


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--wikitext-snapshot",
        required=True,
        metavar="YYYY-MM",
        help=(
            "The wmf.mediawiki_wikitext_current hive " "partition in the format YYYY-MM"
        ),
    )
    parser.add_argument(
        "--item-page-link-snapshot",
        required=True,
        metavar="YYYY-MM-DD",
        help=(
            "The wmf.wikidata_item_page_link hive partition " "in the format YYYY-MM-DD"
        ),
    )
    parser.add_argument(
        "--output",
        required=True,
        metavar="/complete/path/to/file",
        help="The parquet file path to write the output to",
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "--wp-codes",
        nargs="*",
        dest="wiki_dbs",
        metavar="wp-code",
        type=wp_code_to_wiki_db,
        help=(
            "Wikipedia codes to filter articles by, "
            "separated by spaces e.g. ar en zh-yue"
        ),
    )
    group.add_argument(
        "--wp-codes-file",
        metavar="/complete/path/to/file",
        dest="wiki_dbs",
        help=(
            "The json file containing list of " "Wikipedia codes to filter articles by"
        ),
        type=load_wp_codes,
    )
    args = parser.parse_args()
    spark = SparkSession.builder.getOrCreate()

    images_df = get_article_images(
        spark,
        args.wikitext_snapshot,
        args.item_page_link_snapshot,
        args.wiki_dbs,
    )
    images_df.write.partitionBy("wiki_db").mode("overwrite").format("parquet").save(
        args.output
    )

    spark.stop()


if __name__ == "__main__":
    main()
