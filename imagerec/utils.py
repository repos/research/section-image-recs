import json

from typing import List


def wp_code_to_wiki_db(wp_code: str) -> str:
    return wp_code.replace("-", "_") + "wiki"


def load_wp_codes(json_file_path: str) -> List[str]:
    with open(json_file_path) as f:
        wp_codes = json.load(f)
        return [wp_code_to_wiki_db(code) for code in wp_codes]
